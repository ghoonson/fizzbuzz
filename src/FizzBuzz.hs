module FizzBuzz where

data Fizz = OtherFizz Int | ThreeMultipleFizz Int
data Buzz = OtherBuzz Int | ThreeMultipleBuzz Int | FiveMultipleBuzz Int | FifteenMultipleBuzz Int

instance Show Buzz where
  show (OtherBuzz n) = show n
  show (ThreeMultipleBuzz _) = "Fizz"
  show (FiveMultipleBuzz _) = "Buzz"
  show (FifteenMultipleBuzz _) = "FizzBuzz"

fizz :: Int -> Fizz
fizz x = if x `mod` 3 == 0 then ThreeMultipleFizz x else OtherFizz x

buzz :: Fizz -> Buzz
buzz x = case x of
  ThreeMultipleFizz n -> if n `mod` 5 == 0 then FifteenMultipleBuzz n else ThreeMultipleBuzz n
  OtherFizz n -> if n `mod` 5 == 0 then FiveMultipleBuzz n else OtherBuzz n

fizzbuzz :: Int -> String
fizzbuzz x = show $ buzz $ fizz x