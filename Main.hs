module Main where

import FizzBuzz

main = putStrLn $ foldr (\x acc -> if acc == "" then x else x ++ ", " ++ acc) "" (map fizzbuzz [1..100])